## **Ordres d'achat**

Ce projet vise à créer une application Django pour passer des ordres d'achat en utilisant des formulaires en cascade et les stocker dans une base de données MySQL.

### **Prérequis**

- Python3
- Pip3
- Docker
- Docker-compose

### **Installation**

Pour lancer l'application pour la première fois, utilisez la commande suivante:
- docker-compose up -d --build

Pour lancer l'application les fois suivantes, utilisez la commande:
- docker-compose up -d

Pour fermer l'application, utilisez la commande:
- docker-compose down

### **Base de données**

Pour créer ou mettre à jour la base de données, utilisez les commandes suivantes:
- docker-compose run web python manage.py makemigrations
- docker-compose run web python manage.py migrate

### **Superuser**

Pour créer un superuser pour la connexion admin et gérer les autres utilisateurs, utilisez la commande suivante:
- docker-compose run web python manage.py createsuperuser

### **Accès à l'application**

Vous pouvez accéder à l'application à l'adresse suivante:
- [http://localhost:8000/](http://localhost:8000/)

### **Accès à phpMyAdmin**

Vous pouvez accéder à phpMyAdmin à l'adresse suivante:
- [http://localhost:8080/](http://localhost:8080/)

Utilisez le nom d'utilisateur et le mot de passe définis dans le fichier docker-compose.yml.