$(document).ready(function () {
  $("#add-form").on("click", function (e) {
    e.preventDefault();
    const newFormFields = createNewFormFields();
    $(".forms-container").append(newFormFields);
  });

  // Initialize datepicker
  $("#id_date_commande_origine").datepicker({
    format: "dd/mm/yyyy",
    autoclose: true,
    todayHighlight: true,
  });

  // Call function to get exchange rate
  getExchangeRate($("#id_taux_devise"));

  // Serialize and submit the form data as JSON
  $("#order-form").on("submit", function (event) {
    event.preventDefault();

    submitFormDataAsJson();
  });
});

document.addEventListener("DOMContentLoaded", function () {
  const fournisseurSelect = document.getElementById("nom_fournisseur");
  const formatSelect = document.getElementById("format");
  const articleSelect = document.getElementById("id_article");

  fournisseurSelect.addEventListener("change", function () {
    const fournisseurId = this.value;

    const codeArticleElement = $("#code_article");
    const colisageAchatElement = $("#colisage_achat");
    const portEmbarquementElement = $("#port_embarquement");
    const eanCartonElement = $("#ean_carton");
    const nomenclatureElement = $("#nomenclature");
    const ean13Element = $("#ean13");
    const origineFournisseurElement = $("#origine_fournisseur");
    updateFormatOptions(
      fournisseurId,
      formatSelect,
      articleSelect,
      codeArticleElement,
      colisageAchatElement,
      portEmbarquementElement,
      eanCartonElement,
      nomenclatureElement,
      ean13Element,
      origineFournisseurElement
    );
  });

  formatSelect.addEventListener("change", function () {
    const fournisseurId = fournisseurSelect.value;
    const formatId = this.value;

    const codeArticleElement = $("#code_article");
    const colisageAchatElement = $("#colisage_achat");
    const portEmbarquementElement = $("#port_embarquement");
    const eanCartonElement = $("#ean_carton");
    const nomenclatureElement = $("#nomenclature");
    const ean13Element = $("#ean13");
    const origineFournisseurElement = $("#origine_fournisseur");

    updateArticleOptions(
      fournisseurId,
      formatId,
      articleSelect,
      codeArticleElement,
      colisageAchatElement,
      portEmbarquementElement,
      eanCartonElement,
      nomenclatureElement,
      ean13Element,
      origineFournisseurElement
    );
  });

  articleSelect.addEventListener("change", function () {
    const articleId = this.value;
    const codeArticleElement = $("#code_article");
    const colisageAchatElement = $("#colisage_achat");
    const portEmbarquementElement = $("#port_embarquement");
    const eanCartonElement = $("#ean_carton");
    const nomenclatureElement = $("#nomenclature");
    const ean13Element = $("#ean13");
    const origineFournisseurElement = $("#origine_fournisseur");
    updateReadonlyInputForElement(
      articleId,
      codeArticleElement,
      colisageAchatElement,
      portEmbarquementElement,
      eanCartonElement,
      nomenclatureElement,
      ean13Element,
      origineFournisseurElement
    );
  });

  // Call updateFormatOptions directly instead of dispatching the change event
  updateFormatOptions(fournisseurSelect.value, formatSelect, articleSelect);
});

function updateFormatOptions(
  fournisseurId,
  formatSelect,
  articleSelect,
  codeArticleElement,
  colisageAchatElement,
  portEmbarquementElement,
  eanCartonElement,
  nomenclatureElement,
  ean13Element,
  origineFournisseurElement
) {
  if (!fournisseurId) return;

  fetch(`/get_filtered_formats_articles/?fournisseur_id=${fournisseurId}`)
    .then((response) => response.json())
    .then((data) => {
      formatSelect.innerHTML = ""; // Clear the format select options
      articleSelect.innerHTML = '<option value="">---------</option>'; // Added default empty option

      const formats = JSON.parse(data.formats);

      // Filter out formats with no articles
      const formatsWithArticles = formats.filter(
        (format) => format.article_count > 0
      );

      // Check if there are formats with articles
      if (formatsWithArticles.length > 0) {
        // Update the code below to use 'formatsWithArticles' instead of 'formats'
        formatsWithArticles.forEach((format, index) => {
          const option = document.createElement("option");
          option.value = format.pk;
          option.innerText = format.nom;
          formatSelect.appendChild(option);
        });

        // If there are formats with articles, update the articles select for the first format
        updateArticleOptions(
          fournisseurId,
          formatsWithArticles[0].pk,
          articleSelect,
          codeArticleElement,
          colisageAchatElement,
          portEmbarquementElement,
          eanCartonElement,
          nomenclatureElement,
          ean13Element,
          origineFournisseurElement
        );
      } else {
        // If there are no formats with articles, hide the format select
        formatSelect.style.display = "none";
      }
    });
}

function updateReadonlyInputForElement(
  articleId,
  codeArticleElement,
  colisageAchatElement,
  portEmbarquementElement,
  eanCartonElement,
  nomenclatureElement,
  ean13Element,
  origineFournisseurElement
) {
  if (!articleId) {
    $(codeArticleElement).val("");
    $(colisageAchatElement).val("");
    $(portEmbarquementElement).val("");
    $(eanCartonElement).val("");
    $(nomenclatureElement).val("");
    $(ean13Element).val("");
    $(origineFournisseurElement).val("");
    return;
  }

  fetch(`/get_article_field/?article_id=${articleId}`)
    .then((response) => response.json())
    .then((data) => {
      $(codeArticleElement).val(data.code);
      $(colisageAchatElement).val(data.colisage_achat);
      $(portEmbarquementElement).val(data.port_embarquement);
      $(eanCartonElement).val(data.ean_carton);
      $(nomenclatureElement).val(data.nomenclature);
      $(ean13Element).val(data.ean13);
      $(origineFournisseurElement).val(data.origine_fournisseur);
    });
}

function updateArticleOptions(
  fournisseurId,
  formatId,
  articleSelect,
  codeArticleElement,
  colisageAchatElement,
  portEmbarquementElement,
  eanCartonElement,
  nomenclatureElement,
  ean13Element,
  origineFournisseurElement
) {
  if (!fournisseurId || !formatId) return;

  fetch(
    `/get_filtered_articles/?fournisseur_id=${fournisseurId}&format_id=${formatId}`
  )
    .then((response) => response.json())
    .then((data) => {
      articleSelect.innerHTML = "";

      const articles = JSON.parse(data.articles);

      // Check if there is articles
      if (articles.length > 0) {
        articles.forEach((article, index) => {
          const option = document.createElement("option");
          option.value = article.pk;
          option.innerText = article.fields.intitule;
          if (index === 0) {
            codeArticleElement.val(article.fields.code);
            colisageAchatElement.val(article.fields.colisage_achat);
            portEmbarquementElement.val(article.fields.port_embarquement);
            eanCartonElement.val(article.fields.ean_carton);
            nomenclatureElement.val(article.fields.nomenclature);
            ean13Element.val(article.fields.ean13);
            origineFournisseurElement.val(article.fields.origine_fournisseur);
          }
          articleSelect.appendChild(option);
        });
      } else {
        const option = document.createElement("option");
        option.value = "";
        option.innerText = "---------";
        articleSelect.appendChild(option);
      }
    });
}

function createNewFormFields() {
  const formFields = $("#form-fields-container .form-card").html();
  const newFormFields = $('<div class="form-card"></div>').append(
    $('<a class="delete-form"><i class="fas fa-trash"></i></a>'),
    $("<fieldset></fieldset>").append($(formFields))
  );

  // Add style for the form container
  newFormFields.css({
    position: "relative",
  });

  // Add style for the delete icon
  newFormFields.find(".delete-form").css({
    position: "absolute",
    top: 5,
    right: 5,
    color: "red",
  });

  const formCount = $(".forms-container .form-card").length;
  newFormFields.find("legend").append(" #" + (formCount + 1));

  newFormFields.find("input, select").each(function (index, element) {
    const originalName = $(element).attr("name");
    const newName = originalName + "_" + formCount + "_" + (index + 1); // Add a unique suffix
    const originalId = $(element).attr("id");
    const newId = originalId + "_" + formCount + "_" + (index + 1); // Add a unique suffix
    $(element).attr("name", newName);
    $(element).attr("id", newId); // Set the new ID
    $(element).val("");
  });
  newFormFields.find("label").each(function (index, element) {
    const originalFor = $(element).attr("for");
    const newFor = originalFor + "_" + formCount + "_" + (index + 2); // Add a unique suffix
    $(element).attr("for", newFor); // Set the new "for" attribute
  });

  const newFournisseurSelect = newFormFields
    .find('[id^="nom_fournisseur"]')
    .get(0);
  const newFormatSelect = newFormFields.find('[id^="format"]').get(0);
  const newArticleSelect = newFormFields.find('[id^="id_article"]').get(0);

  // Find the new field with "id_taux_devise" and call "getExchangeRate()"
  const newTauxDevise = newFormFields.find('[id^="id_taux_devise"]');
  getExchangeRate(newTauxDevise);

  // Find the new field with "id_date_commande_origine" and call the datepicker() method on it.
  const newDateCommandeOrigine = newFormFields.find(
    '[id^="id_date_commande_origine"]'
  );

  newDateCommandeOrigine.datepicker({
    format: "dd/mm/yyyy",
    autoclose: true,
    todayHighlight: true,
  });

  newFournisseurSelect.addEventListener("change", function () {
    const fournisseurId = this.value;
    const newCodeArticleElement = newFormFields.find('[id^="code_article"]');
    const newColisageAchatElement = newFormFields.find('[id^="colisage_achat"]');
    const newPortEmbarquementElement = newFormFields.find('[id^="port_embarquement"]');
    const newEanCartonElement = newFormFields.find('[id^="ean_carton"]');
    const newNomenclatureElement = newFormFields.find('[id^="nomenclature"]');
    const newEan13Element = newFormFields.find('[id^="ean13"]');
    const newOrigineFournisseurElement = newFormFields.find('[id^="origine_fournisseur"]');
    updateFormatOptions(
      fournisseurId,
      newFormatSelect,
      newArticleSelect,
      newCodeArticleElement,
      newColisageAchatElement,
      newPortEmbarquementElement,
      newEanCartonElement,
      newNomenclatureElement,
      newEan13Element,
      newOrigineFournisseurElement
    );
  });

  newFormatSelect.addEventListener("change", function () {
    const fournisseurId = newFournisseurSelect.value;
    const formatId = this.value;

    const newCodeArticleElement = newFormFields.find('[id^="code_article"]');
    const newColisageAchatElement = newFormFields.find('[id^="colisage_achat"]');
    const newPortEmbarquementElement = newFormFields.find('[id^="port_embarquement"]');
    const newEanCartonElement = newFormFields.find('[id^="ean_carton"]');
    const newNomenclatureElement = newFormFields.find('[id^="nomenclature"]');
    const newEan13Element = newFormFields.find('[id^="ean13"]');
    const newOrigineFournisseurElement = newFormFields.find('[id^="origine_fournisseur"]');

    updateArticleOptions(
      fournisseurId,
      formatId,
      newArticleSelect,
      newCodeArticleElement,
      newColisageAchatElement,
      newPortEmbarquementElement,
      newEanCartonElement,
      newNomenclatureElement,
      newEan13Element,
      newOrigineFournisseurElement
    );
  });

  newArticleSelect.addEventListener("change", function () {
    const articleId = this.value;
    const newCodeArticleElement = newFormFields.find('[id^="code_article"]');
    updateReadonlyInputForElement(articleId, newCodeArticleElement);
  });

  // Reset format and article select options
  newFormatSelect.innerHTML = '<option value="">---------</option>';
  newArticleSelect.innerHTML = '<option value="">---------</option>';

  return newFormFields;
}

function showFlashMessage(message, type) {
  const flashMessage = $(`<div class="flash-message ${type}">${message}</div>`);
  $("body").append(flashMessage);
  setTimeout(() => {
    flashMessage.fadeOut(300, () => {
      flashMessage.remove();
    });
  }, 3000);
}

function submitFormDataAsJson() {
  const formGroups = $(".forms-container .form-card fieldset.input-group");
  const allFormData = [];

  formGroups.each(function (index, formGroup) {
    const inputs = $(formGroup).find("input, select");

    // Wrap the fieldset inside a form element
    const tempForm = document.createElement("form");
    tempForm.appendChild(formGroup.cloneNode(true));

    // Remove the suffix added to "name" and "id" attributes
    $(tempForm)
      .find("input, select")
      .each(function () {
        const originalName = $(this).attr("name");
        const originalId = $(this).attr("id");

        if (originalName) {
          const newName = originalName.replace(/_\d+_\d+$/, "");
          $(this).attr("name", newName);
        }

        if (originalId) {
          const newId = originalId.replace(/_\d+_\d+$/, "");
          $(this).attr("id", newId);
        }
      });

    const formDataObj = Object.fromEntries(new FormData(tempForm).entries());

    // Add the "nom_fournisseur" field to the formDataObj object
    const fournisseurSelect = $(formGroup).find('[id^="nom_fournisseur"]');
    if (fournisseurSelect.length) {
      formDataObj["nom_fournisseur"] = fournisseurSelect.val();
    }

    allFormData.push(formDataObj);
  });

  const csrfToken = $('input[name="csrfmiddlewaretoken"]').first().val();
  const url = "/process_order_form/";

  fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "X-CSRFToken": csrfToken,
    },
    body: JSON.stringify(allFormData),
  })
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        throw response;
      }
    })
    .then((data) => {
      console.log("Success:", data);

      // Show the success flash message
      const flashMessageContainer = $("#flash-message-container");
      const flashMessage = $("#flash-message");
      flashMessage.text("Les données ont été soumises avec succès.");
      flashMessageContainer.removeClass("d-none");

      // Hide the flash message after 3 seconds and redirect the user to the order form page
      setTimeout(() => {
        flashMessageContainer.addClass("d-none");
        window.location.href = "/order_form/";
      }, 3000);
    })
    .catch(async (errorResponse) => {
      const errorData = await errorResponse.json();
      console.log("Error:", errorData);

      // Create an array of strings that contain the field names and error messages
      const errorMessages = [];
      for (const [field, errors] of Object.entries(errorData.errors)) {
        const fieldName = field.replace("id_", ""); // Remove the "id_" prefix from the field name
        const errorMessage = errors.map((error) => error.message).join(", ");
        errorMessages.push(`${fieldName}: ${errorMessage}`);
      }

      // Open the error dialog box and display the error messages
      showErrorDialog(errorMessages);
    });
}

function showErrorDialog(errorMessages) {
  const dialog = document.createElement("div");
  dialog.classList.add("modal");
  dialog.tabIndex = -1;
  dialog.setAttribute("aria-labelledby", "error-dialog-title");
  dialog.setAttribute("aria-hidden", "true");

  const dialogContent = `
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="error-dialog-title">Erreurs dans un des formulaire</h5>
        <button type="button" class="btn-close close-btn" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <ul>
          ${errorMessages.map((message) => `<li>${message}</li>`).join("")}
        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary close-btn">Fermer</button>
      </div>
    </div>
  </div>
  `;

  dialog.innerHTML = dialogContent;
  document.body.appendChild(dialog);

  // Inizializza e apri il dialog di Bootstrap
  const bootstrapDialog = new bootstrap.Modal(dialog);
  bootstrapDialog.show();

  // Add listener for "Close" button and top-right button
  const closeButtons = dialog.querySelectorAll(".close-btn");
  closeButtons.forEach((button) => {
    button.addEventListener("click", () => {
      bootstrapDialog.hide();
    });
  });

  // Remove dialog from DOM when it's hidden
  dialog.addEventListener("hidden.bs.modal", () => {
    document.body.removeChild(dialog);
  });
}

$(document).on("click", ".delete-form", function () {
  if (confirm("Are you sure you want to delete this form?")) {
    $(this).closest(".form-card").remove();
  }
});

function getExchangeRate(element) {
  const url = "https://api.exchangerate-api.com/v4/latest/USD";

  fetch(url)
    .then((response) => response.json())
    .then((data) => {
      const exchangeRate = data.rates.EUR;
      element.val(exchangeRate.toFixed(2));
    })
    .catch((error) => {
      console.error("Error fetching exchange rate:", error);
    });
}
