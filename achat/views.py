from django.shortcuts import render
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.contrib.auth import logout
from django.shortcuts import redirect
from .models import Fournisseur, Commande, Article, Format
from .forms import OrderForm
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
import json
from django.http import QueryDict
from time import time

def user_login(request):
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect("/order_form/")
        else:
            return HttpResponse("Identifiants invalides.")
    return render(request, "achat/login.html")

@csrf_exempt
@login_required
def process_order_form(request):
    if request.method == "POST":
        # Convert JSON to QueryDict
        json_data = json.loads(request.body.decode("utf-8"))
        # print("json_data:", json_data)

        # Initialize a list to store the individual form data dicts
        data_list = []

        if isinstance(json_data, list):  # Check if json_data is a list
            for form_data in json_data:
                form_data_dict = QueryDict('', mutable=True)
                
                # Deserialize form_data if it's a string
                if isinstance(form_data, str):
                    form_data = json.loads(form_data)
                    
                # Iterate over the form_data dictionary and update form_data_dict
                for key, value in form_data.items():
                    form_data_dict[key] = value

                data_list.append(form_data_dict)
        else:  # If json_data is a single JSON object
            form_data_dict = QueryDict('', mutable=True)
            
            # Iterate over the json_data dictionary and update form_data_dict
            for key, value in json_data.items():
                form_data_dict[key] = value

            data_list.append(form_data_dict)

        # Initialize a list to store the Commande objects
        commandes = []

        # Initialize a variable to store any errors
        errors = {}

        # Iterate over the received form data
        for data in data_list:
            # Create a new instance of the form with the received data
            form = OrderForm(data)

            # Check if the form is valid
            if form.is_valid():
                
                format_to_insert = ""
                article_to_insert = ""
                formatObj = form.cleaned_data['format']
                articleObj = form.cleaned_data['article']
                if form.cleaned_data['format'] is not None and formatObj:
                    format_to_insert = formatObj.nom
                if form.cleaned_data['article'] is not None and articleObj:
                    article_to_insert = articleObj.intitule

                # Create a new Commande object with the form data
                commande = Commande(
                    utilisateur=request.user,
                    code_article=form.cleaned_data['code_article'],
                    nom_fournisseur=form.cleaned_data['nom_fournisseur'].nom,
                    format=format_to_insert,
                    article=article_to_insert,
                    quantite=form.cleaned_data['quantite'],
                    prix_achat_net=form.cleaned_data['prix_achat_net'],
                    colisage_achat=form.cleaned_data['colisage_achat'],
                    date_commande_origine=form.cleaned_data['date_commande_origine'],
                    incoterm=form.cleaned_data['incoterm'],
                    taux_devise=form.cleaned_data['taux_devise'],
                    prix_devise=form.cleaned_data['prix_devise'],
                    port_embarquement=form.cleaned_data['port_embarquement'],
                    ean_carton=form.cleaned_data['ean_carton'],
                    nomenclature=form.cleaned_data['nomenclature'],
                    ean13=form.cleaned_data['ean13'],
                    etat_ordre=form.cleaned_data['etat_ordre'],
                    origine_fournisseur=form.cleaned_data['origine_fournisseur']
                )

                # Add the Commande object to the list
                commandes.append(commande)
            else:
                # Add the current form's errors to the total errors
                errors.update(form.errors.get_json_data())

        # Check if there are any errors
        if errors:
            # Send a JSON response to the client with the error
            return JsonResponse({"errors": errors}, status=400)
        else:
            # Generate a unique numero_ordre using the current timestamp
            numero_ordre = int(time())

            # Save all the Commande objects in the database
            for commande in commandes:
                commande.numero_ordre = numero_ordre

                print(commande)
                print("---------------------------------------------")
                commande.save()

            # Send a JSON response to the client
            response_data = {
                "status": "success",
                "message": "Form data submitted successfully.",
            }
            return JsonResponse(response_data)
    return render(request, "achat/order_form.html", {'form': OrderForm()})

@login_required
def order_form(request):
    if request.method == 'GET':
        form = OrderForm()
        return render(request, "achat/order_form.html", {'form': form})

@login_required
def user_logout(request):
    logout(request)
    return redirect("login")

def autocomplete_code_articles(request):
    if request.headers.get('X-Requested-With') == 'XMLHttpRequest':
        query = request.GET.get('term', '')
        form = OrderForm()
        form.fields['code_article'].queryset = Article.objects.filter(
            code__icontains=query)
        results = []
        for code_article in form.fields['code_article'].queryset:
            code_article_json = {}
            code_article_json['label'] = code_article.code
            code_article_json['value'] = code_article.pk
            results.append(code_article_json)
        return JsonResponse(results, safe=False)
    return HttpResponse("Requête non valide.")

def get_filtered_formats_articles(request):
    fournisseur_id = request.GET.get('fournisseur_id')
    fournisseur = Fournisseur.objects.get(id=fournisseur_id)

    # Use the reverse relationship between Fournisseur and Format
    formats = fournisseur.format_set.all()

    # Create a list of format dictionaries including the article count
    format_dicts = []
    articles = Article.objects.none()

    for format in formats:
        # Filter articles for the current fournisseur and format
        articles_for_format = Article.objects.filter(fournisseur=fournisseur, format=format)

        # Add the format to the format_dicts list if there are articles for it
        if articles_for_format.exists():
            format_dicts.append({"pk": format.pk, "nom": format.nom, "article_count": articles_for_format.count()})

            # If it's the first format, use its articles for the response
            if not articles:
                articles = articles_for_format

    data = {
        'formats': json.dumps(format_dicts),
        'articles': serializers.serialize('json', articles)
    }
    return JsonResponse(data)

def get_filtered_articles(request):
    fournisseur_id = request.GET.get('fournisseur_id')
    format_id = request.GET.get('format_id')
    articles = Article.objects.filter(
        fournisseur__id=fournisseur_id, format__id=format_id)
    data = {
        'articles': serializers.serialize('json', articles)
    }
    return JsonResponse(data)

def get_article_field(request):
    article_id = request.GET.get('article_id', None)

    if article_id is not None:
        try:
            article = Article.objects.get(pk=article_id)
            article_data = {
                'code': article.code,
                'colisage_achat': article.colisage_achat,
                'port_embarquement': article.port_embarquement,
                'ean_carton': article.ean_carton,
                'nomenclature': article.nomenclature,
                'ean13': article.ean13,
                'origine_fournisseur': article.origine_fournisseur,
            }
            return JsonResponse(article_data)
        except Article.DoesNotExist:
            return JsonResponse({"error": "Article not found."}, status=404)
    else:
        return JsonResponse({"error": "Article ID missing."}, status=400)
