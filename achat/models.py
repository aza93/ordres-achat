from django.db import models
from django.contrib.auth.models import User

class Fournisseur(models.Model):
    nom = models.CharField(max_length=255)
    
    def __str__(self):
        return self.nom

class Format(models.Model):
    nom = models.CharField(max_length=255)
    fournisseurs = models.ManyToManyField(Fournisseur)

    def article_count(self):
        return self.article_set.count()
    
    def __str__(self):
        return self.nom

class Article(models.Model):
    code = models.IntegerField()
    fournisseur = models.ForeignKey(Fournisseur, on_delete=models.CASCADE)
    format = models.ForeignKey(Format, on_delete=models.CASCADE)
    intitule = models.CharField(max_length=255)
    colisage_achat = models.CharField(max_length=255, default='', blank=True)
    port_embarquement = models.CharField(max_length=255, default='', blank=True)
    ean_carton = models.CharField(max_length=255, default='', blank=True)
    nomenclature = models.CharField(max_length=255, default='', blank=True)
    ean13 = models.CharField(max_length=255, default='', blank=True)
    origine_fournisseur = models.CharField(max_length=255, default='', blank=True)
    
    def __str__(self):
        return self.intitule

class Commande(models.Model):
    numero_ordre = models.IntegerField()
    utilisateur = models.ForeignKey(User, on_delete=models.CASCADE)
    code_article = models.CharField(max_length=255, default='')
    nom_fournisseur = models.CharField(max_length=255, default='')
    format = models.CharField(max_length=255, default='', blank=True)
    article = models.CharField(max_length=255, default='', blank=True)
    quantite = models.IntegerField()
    prix_achat_net = models.DecimalField(max_digits=10, decimal_places=2)
    colisage_achat = models.CharField(max_length=255)
    date_commande_origine = models.DateField()
    incoterm = models.CharField(max_length=255)
    taux_devise = models.DecimalField(max_digits=10, decimal_places=2)
    prix_devise = models.DecimalField(max_digits=10, decimal_places=2)
    port_embarquement = models.CharField(max_length=255)
    ean_carton = models.CharField(max_length=255)
    nomenclature = models.CharField(max_length=255)
    ean13 = models.CharField(max_length=255)
    etat_ordre = models.CharField(max_length=255, choices=[("prod", "prod"), ("confirme", "confirme"), ("annule", "annule")])
    origine_fournisseur = models.CharField(max_length=255)

    def __str__(self):
        return (
            f"Numero d'ordre: {self.numero_ordre}\n"
            f"Utilisateur: {self.utilisateur}\n"
            f"Code Article: {self.code_article}\n"
            f"Nom fournisseur: {self.nom_fournisseur}\n"
            f"Format: {self.format}\n"
            f"Article: {self.article}\n"
            f"Quantite: {self.quantite}\n"
            f"Prix Achat Net: {self.prix_achat_net}\n"
            f"Colisage Achat: {self.colisage_achat}\n"
            f"Date Commande Origine: {self.date_commande_origine}\n"
            f"Incoterm: {self.incoterm}\n"
            f"Taux Devise: {self.taux_devise}\n"
            f"Prix Devise: {self.prix_devise}\n"
            f"Port Embarquement: {self.port_embarquement}\n"
            f"EAN Carton: {self.ean_carton}\n"
            f"Nomenclature: {self.nomenclature}\n"
            f"EAN13: {self.ean13}\n"
            f"Etat Ordre: {self.etat_ordre}\n"
            f"Origine Fournisseur: {self.origine_fournisseur}\n"
        )
