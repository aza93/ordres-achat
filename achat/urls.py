from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', views.order_form, name='home'),
    path('login/', views.user_login, name='login'),
    path('order_form/', views.order_form, name='order_form'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('get_article_field/', views.get_article_field, name='get_article_field'),
    path('get_filtered_formats_articles/', views.get_filtered_formats_articles, name='get_filtered_formats_articles'),
    path('get_filtered_articles/', views.get_filtered_articles, name='get_filtered_articles'),
    path('process_order_form/', views.process_order_form, name='process_order_form'),
]
