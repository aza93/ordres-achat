from django import forms
from .models import Fournisseur, Article, Format


class OrderForm(forms.Form):
    code_article = forms.CharField(
        label="Code Article:",
        widget=forms.TextInput(attrs={'class': 'form-control', 'readonly': 'readonly'}),
        required=False,
    )
    nom_fournisseur = forms.ModelChoiceField(
        queryset=Fournisseur.objects.all(),
        empty_label="-- sélectionnez un fournisseur --",
        label="Nom Fournisseur:",
        required=False,
        widget=forms.Select(attrs={'class': 'form-control'}),
    )
    format = forms.ModelChoiceField(
        queryset=Format.objects.all(),
        empty_label="-- sélectionnez un format --",
        label="Format",
        required=False,
    )
    article = forms.ModelChoiceField(
        queryset=Article.objects.all(),
        empty_label="-- sélectionnez un article --",
        label="Article:",
        required=False,
        widget=forms.Select(attrs={'class': 'form-control'})
    )
    quantite = forms.IntegerField(
        label="Quantité:",
        widget=forms.NumberInput(attrs={'class': 'form-control'})
    )
    prix_achat_net = forms.DecimalField(
        label="Prix d'achat net:",
        widget=forms.NumberInput(attrs={'class': 'form-control'})
    )
    colisage_achat = forms.CharField(
        label="Colisage achat:",
        widget=forms.TextInput(attrs={'class': 'form-control', 'readonly': 'readonly'})
    )
    date_commande_origine = forms.DateField(
        label="Date commande origine:",
        input_formats=['%d/%m/%Y'],
        widget=forms.DateInput(attrs={'class': 'form-control'}),
    )
    incoterm = forms.CharField(
        label="Incoterm:",
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )
    taux_devise = forms.CharField(widget=forms.TextInput())
    prix_devise = forms.DecimalField(
        label="Prix en devise:",
        widget=forms.NumberInput(attrs={'class': 'form-control'})
    )
    port_embarquement = forms.CharField(
        label="Port d'embarquement:",
        widget=forms.TextInput(attrs={'class': 'form-control', 'readonly': 'readonly'})
    )
    ean_carton = forms.CharField(
        label="EAN carton:",
        widget=forms.TextInput(attrs={'class': 'form-control', 'readonly': 'readonly'})
    )
    nomenclature = forms.CharField(
        label="Nomenclature:",
        widget=forms.TextInput(attrs={'class': 'form-control', 'readonly': 'readonly'})
    )
    ean13 = forms.CharField(
        label="EAN13:",
        widget=forms.TextInput(attrs={'class': 'form-control', 'readonly': 'readonly'})
    )
    etat_ordre = forms.CharField(
        label="Etat de l'ordre:",
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )
    origine_fournisseur = forms.CharField(
        label="Origine fournisseur:",
        widget=forms.TextInput(attrs={'class': 'form-control', 'readonly': 'readonly'})
    )
